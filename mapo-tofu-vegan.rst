Mapo Tofu mit Broccoli
======================

Zutaten
--------

für 2 Portionen

* 1 tsp Szechuanpfeffer
* 1 Block Soft Tofu
* 1 tbsp Doubanjiang (Chili Bean Paste)
* 1 Knoblauchzehe
* Ingwer
* Frühlingszwiebeln
* getrocknete Shiitake
* 1 Dose Kichererbsen
* 1 tbsp Dou Chi (Fermented Black Beans)
* Chillipulver
* 1-2 tbsp Sojasoße
* 1 tbsp Dunkler Essig
* 1 tbsp Stärke
* 1 tbsp Öl
* 300g Broccoli

Zubereitung:
------------

1.  Pilze einweichen
2.  Kichererbsen zerkleinern
3.  Pilze abgießen, Wasser aufheben
4.  Pilze, ausquetschen und klein schneiden
5.  Pilze und Kichererbsen mischen
6.  Szechuanpfeffer toasten und mahlen
7.  Tofu in 2cm große Würfel schneiden und in kochendes Salzwasser legen (2-3min)
8.  Broccoli auftauen / kochen
9.  Ingwer, Knoblauch und Frühlingszwiebeln schneiden
10. Pilze und Kichererbsen im Wok anbraten
11. Knoblauch, Ingwer und Gewürze im Wok anbraten
12. Doubanjiang und Dou Chi hinzufügen
13. Pilzwasser hinzufügen
14. Broccoli hinzufügen
15. Stärke in Wasser (2 tbsp) lösen und damit Soße eindicken
16. Tofu hinzufügen
17. Auf Teller servieren und mit Frühlingszwiebeln garnieren

Die Nährwerte
-------------

Pro Portion:

* 659 kcal
* 28g Fett
* 50g Kohlenhydrate
* 42g Protein
