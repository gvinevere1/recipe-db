Scharfes Hähnchen mit Karotten-Feldsalat
==========================================

.. image:: images/scharfes-haehnchen.jpg
        :width: 400


Zutaten:
---------
für 2 Personen

* 250 gr. Hähnchenbrustfilet
* 4 gr. Gewürzmischung Piri Piri
* 1 rote Chilischote
* 1 Knoblauchzehe
* 600 gr. festk. Kartoffeln
* 1 Limette
* 10 gr. Petersilie
* 100 gr. Joghurth
* 2 Karotten
* 75 gr. Feldsalat
* 4 gr. Schwarzkümmel
* 8 gr. Honig
* Öl
* Salz
* Pfeffer


Zubereitung:
------------

1. Heize den Backofen auf 220 C Ober-/Unterhitze (200 C Umluft) vor.
2. Kartoffeln schälen und in ca. 2 cm große Stücke schneiden.
3. Kartoffelwürfel auf ein mit Backpapier belegtes Blech geben (etwas Platz für das Fleisch lassen). Mit Salz und Pfeffer würzen. 
4. Dann auf mittlerer Schiene im Backofen 25 - 30 Min. backen, bis die Kartoffeln weich sind.
5. Chili längs halbieren, entkernen und Chilihälften fein hacken (Achtung: scharf!).
6. Knoblauch abziehen und in eine kleine Schüssel pressen.
7. Gewürzmischung "Piri Piri" und nach Belieben etwas gehackte Chili hinzufügen.
8. Mit 1 EL Öl verrühren und mit Salz und Pfeffer abschmecken.
9. Hähnchenbrust auf beiden Seiten salzen.
10. Anschließend Chilimarinade auf den Filets verteilen und diese für die letzten 12 - 14 Min. der Backzeit der Kartoffelwürfel (diese bei dieser Gelgenheit einmal wenden) mit in den Backofen geben und backen, bis die Hähnchenbrust innen nicht mehr rosa ist.
11. Limette vierteln. 
12. Karotten schälen und grob in eine große Schüssel raspeln.
13. Mit Saft von 1 Limettenviertel, 1 EL Öl, Honig, Schwarzkümmel, Salz und Pfeffer vermischen und kurz beiseitestellen.
14. Blätter der Petersilie abzupfen und fein hacken.
15. In einer kleinen Schüssel gehackte Petersilie mit Joghurth, Salz, Pfeffer vermischen und mit einem Spritzer Limettensaft abschmecken.
16. Feldsalat unter die Karotten heben.
17. Schafe Hähnchenbrust, Kartoffelwürfel und Karottensalat auf Teller verteilen und zusammen mit dem Petersilienjoghurt genießen.


Nährwerte (pro Portion):
-------------------------
Brennwert:      591 kcal
Fett:           19,35 g
Carbs:          61,51 g
Eiweiß:         39,81 g
