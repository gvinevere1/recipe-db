Kidneybohnen Brownies
======================

Zutaten
--------


* 1 Dose (250g) Kidney-Bohnen
* 50g körniger Frischkäse (Halbfettstufe) oder Magerquark
* 30g Honig
* 2 Scoops Impact Whey Protein in „Schokolade“
* 30g Kakaopulver, entölt
* 1 Teelöffel Backpulver
* 5-7 Tropfen FlavDrops in “Schokolade”
* 30g Schoko-Nibbs
* 25g Paranüsse und/oder Erdnussbutter
* Süßstoff nach Belieben



Zubereitung:
------------

1. Zuerst wird der Ofen auf 150 °C vorgeheizt und alle Zutaten, bis auf die Schoko-Nibbs – in einer großen Schüssel zusammengemischt. Entweder nutzt du hierfür den Handmixer, einen normalen Mixer oder die Küchemaschine – am Ende soll daraus der Muffinteig entstehen! 
2. Verteile den Teig in einem Backblech, welches du vorher mit Backpapier ausgelegt hast.
3. Backe das Ganze für 10-12 Minuten und lasse die Brownies anschließend etwas abkühlen.
4. Schneide die Stücke in die gewünschte Größe


Die Nährwerte (ganzes Blech mit Schoko-Nibbs)
-----------------------------------------------

* 1087 kcal
* 77,8 g Protein
* 72,9 g Kohlenhydrate
* 46,5 g Fett
